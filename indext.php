<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Velchala Kondal Rao</title>
    <?php 
        include 'includes/styles.php';
        include 'includes/arrayObjects.php';
    ?>
</head>
<body>
    <div class="links">
        <a id="a2" data-id="a2" href="#top"><span>Intro</span></a>
        <a id="a1" data-id="a1" href="#two"><span>VSP</span></a>
        <a id="a4" data-id="a4" href="#three"><span>Info</span></a>
        <a id="a3" data-id="a3" href="#four"><span>Career</span></a>
        <a id="a5" data-id="a5" href="#five"><span>Books</span></a>
        <a id="a7" data-id="a7" href="#seven"><span>Gallery</span></a>
        <a id="a6" data-id="a6" href="#six"><span>Contact</span></a>       
    </div>

    <!-- header -->
    <header class="fixed-top">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="indext.php">
                    <img src="img/logo.svg" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                   
                    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">ENGLISH</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="https://www.velchala.com/publications">పుస్తక రచనలు </a>
                        </li>                   
                    </ul>
                    
                </div>
            </nav>
        </div>
    </header>
    <!--/ header -->

    <!-- wrapper main -->
    <div class="wrapperMain">
        <!-- section introduction -->
		<div class="section bannerSection" id="top">
            <!-- container fluid -->
            <div class="container-fluid">
                <!-- row -->
                <div class="row">
                    <!-- left col -->
                    <div class="col-sm-6 text-center">
                        <img src="img/vkraoNew.svg" alt="" class="img-fluid vkimg">
                    </div>
                    <!--/ left col -->

                    <!-- right col -->
                    <div class="col-sm-6 align-self-center">
                        <article class="vkrArticle">
                            <h1 class="h1"><span>సుస్వాగతం</span></h1>
                            <h2 class="h1">వెల్చాల </h2>
                            <p>తెలుగు అకాడెమీ మాజీ సంచాలకులు,'జయంతి' సాహిత్య సాంస్కృతిక త్రైమాసిక పత్రిక సంపాదకులు, పోయెట్రీ సొసైటీ అఫ్ ఇండియా జీవిత సభ్యులు, పోయెట్రీ సొసైటీ అఫ్ హైదరాబాద్ సభ్యులు, ఇండియన్ ఇన్స్టిట్యూట్ అఫ్ పబ్లిక్ అడ్మినిస్ట్రేషన్ పర్మనెంట్ సభ్యులు, ఫోరమ్ ఫర్ హయ్యర్ ఎడ్యుకేషన్, హైదరాబాద్ ఉపాధ్యక్షులు, తెలుగు,ఇంగ్లీష్, ఉర్దూ భాషల్లో రచయిత, అనువాదకుడు, సంకలనకర్త. </p>
                            <!-- <p class="pt-3">
                                <a href="javascript:void(0)" class="ybtn">Know More</a>
                            </p> -->
                        </article>
                    </div>
                    <!--/ right col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container fluid -->
        </div>
        <!-- /section introduction -->

        <!-- section -->
        <div class="section bannerSection" id="two">
            <!-- container fluid -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- left col -->
                    <div class="col-sm-4 text-center order-sm-last">
                        <img src="img/vsp-png.png" alt="" class="img-fluid">
                    </div>
                    <!--/ left col -->

                    <!-- right col -->
                    <div class="col-sm-8 align-self-center">
                        <article>
                            <h2 class="h2"><span>వ్యవస్థాపకులు</span></h2>
                            <h2 class="h2">విశ్వనాథ సాహిత్య పీఠం </h2>
                            <p>ిశ్వనాథ సత్యనారాయణ పేరుతో "విశ్వనాథ సాహిత్య పీఠం" జులై 2003లో ప్రారంభమైంది. కవులు, రచయితల పేరు మీద సాహితీ సంస్థలు, పీఠాలు నెలకొల్పడం సహజమే. అయితే విశ్వనాథ మరణం తరువాత మూడు దశాబ్దాలకు ఈ పీఠం ఏర్పడడం వెనుక దాదాపు మూడు దశాబ్దాల పాటు సాగిన సుదీర్ఘ అంతర్మథన, ఆలోచనల, సంప్రదింపుల సమాహారం ఉంది.</p>
                            <p class="pt-2">
                                <a href="javascript:void(0)" class="ybtn" data-toggle="modal" data-target="#vspMore">మరెంత సమాచారం </a>
                            </p>
                        </article>
                    </div>
                    <!--/ right col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container fluid -->
        </div>
        <!--/ section -->

        <!-- section -->
        <div class="section infoSection" id="three">
            <!-- container fluid -->
            <div class="container">
                <!-- row -->
                <dv class="row">
                    <!-- col -->
                    <div class="col-md-8">
                        <article>
                            <h2 class="h3 fyellow">లైఫ్ టైం అచివ్మెంట్  అవార్డు గ్రహీత</h2>
                            <p>సమర్థవంతమైన విద్యావేత్త మరియు గొప్ప రచయిత డాక్టర్ వెల్చల కొండల్ రావు, 2015 ఏప్రిల్ 20 న హైదరాబాద్ లోని రవీంద్ర భారతిలో జరిగిన కార్యక్రమంలో జీవిత భాషా సాంస్కృతిక పురస్కారాన్ని సత్కరించింది. </p>
                        </article>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-4 text-md-right">
                        <h2 class="h3 fyellow">April 20, 2015</h2>                        
                    </div>
                    <!--/ col -->
                </dv>
                <!--/row -->

                <!-- row -->
                <dv class="row py-2">
                    <!-- col -->
                    <div class="col-md-6">
                        <article>
                            <h2 class="h4 fyellow">్యక్తిగత వివరాలు</h2>
                            <p>1932 జూలై 21 న తెలంగాణలోని కరీంనగర్‌లో జన్మించారు. వి.మిర్మల దేవిని వివాహం చేసుకున్నారు. పి. రామదేవి మరియు ఆర్. ఉమా దేవి మరియు ఒక కుమారుడు అగం రావు వెల్చాలా అనే ఇద్దరు కుమార్తెలతో ఆశీర్వదించారు</p>
                        </article>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-md-6">
                        <article>
                            <h2 class="h4 fyellow">విద్యా నేపథ్యం</h2>
                            <p class="pb-0 mb-0">ఎం. కాం. ఉస్మానియా విశ్వవిద్యాలయం నుండి.</p>
                            <p>ఎల్.ఎల్.బి. లక్నో విశ్వవిద్యాలయం నుండి</p>
                        </article>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-md-6">
                        <article>
                            <h2 class="h4 fyellow">ఎడిటర్ - పుస్తకాల కంపైలర్</h2>
                            <p class="pb-0 mb-0">ఇంగ్లీష్ మరియు తెలుగు భాషలలో 10 పుస్తకాలను సవరించారు, "తెలంగాణ పోరాటం కోసం గుర్తింపు" అనే పుస్తకంతో సహా</p>
                        </article>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-md-6">
                        <article>
                            <h2 class="h4 fyellow">కవి</h2>
                            <p class="pb-0 mb-0">తెలుగు, ఇంగ్లీష్, ఉర్దూ భాషలలో 
                            </p>
                        </article>
                    </div>
                    <!--/ col -->                   
                </dv>
                <!--/row -->
            </div>
            <!--/ container fluid -->
        </div>
        <!--/ section -->

        <!-- section -->
        <div class="section" id="four">
            <!-- container-->
            <div class="container">
                <!-- row -->
                <dv class="row">
                    <!-- col -->
                    <div class="col-md-8">
                        <article>
                            <h2 class="h3 fyellow ">My Career</h2>
                            <h2 class="h3 fwhite">History & Timeline</h2>                            
                        </article>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-4 text-md-right">
                        <h2 class="h3 fyellow">75</h2>
                        <h5>Years</h5>
                    </div>
                    <!--/ col -->
                </dv>
                <!--/row -->

                <!-- time line -->
                <section class="cd-horizontal-timeline">
                    <div class="timeline">
                        <div class="events-wrapper">
                            <div class="events">
                                <ol>
                                    <li><a href="#0" data-date="01/01/1935" class="selected"><span>1932</span></a></li>
                                    <li><a href="#0" data-date="01/01/1950"><span>1937</span></a></li>
                                    <li><a href="#0" data-date="01/01/1965"><span>1945-47</span></a></li>
                                    <li><a href="#0" data-date="01/01/1980"><span>1950-55</span></a></li>
                                    <li><a href="#0" data-date="01/01/1995"><span>1956-65</span></a></li>
                                    <li><a href="#0" data-date="01/01/2010"><span>1974</span></a></li>
                                    <li><a href="#0" data-date="01/01/2025"><span>1980</span></a></li>
                                    <li><a href="#0" data-date="01/01/2040"><span>1990</span></a></li> 
                                </ol>                
                                <span class="filling-line" aria-hidden="true"></span>
                            </div> <!-- .events -->
                        </div> <!-- .events-wrapper -->
                
                        <ul class="cd-timeline-navigation">
                            <li><a href="#0" class="prev inactive">Prev</a></li>
                            <li><a href="#0" class="next">Next</a></li>
                        </ul> <!-- .cd-timeline-navigation -->
                    </div> <!-- .timeline -->
                
                    <div class="events-content">
                        <ol>
                            <li class="selected" data-date="01/01/1935">
                                <h4 class="h4 fyellow d-none">21 July 1932 </h4>
                                <h3 class="h3">1932 జూలై 21 </h3>
                                <p>1932 జూలై 21 న తెలంగాణలోని కరీంనగర్‌లో జన్మించారు</p>

                            </li>                
                
                            <li data-date="01/01/1950">
                                <h4 class="h4 fyellow d-none">10 March 1954 </h4>
                                <h3 class="h3">Hఫై చదువులు</h3>
                                <div class="row"></div>
                                <p class="listgroup">
                                    <span>ఎం. కాం. ఉస్మానియా విశ్వవిద్యాలయం నుండి</span>
                                    <span>ఎల్.ఎల్.బి. లక్నో విశ్వవిద్యాలయం నుండి</span>
                                </p>                               
                            </li>
                
                            <li data-date="01/01/1965">
                                <h4 class="h4 fyellow d-none">1945-1947 </h4>
                                <h3 class="h3">భారత స్వతంత్ర సమర యోధులు </h3>
                                <div class="row"></div>
                                <p class="listgroup">
                                    <span>భారత స్వాతంత్ర్య పోరాటం మరియు హైదరాబాద్ విముక్తి పోరాటంలో భూగర్భంలో పనిచేశారు</span>
                                    <span>1947 ఆగస్టు 7 న హైదరాబాద్ లోని కోటిలో సత్యాగ్రహాన్ని ప్రదర్శించారు</span>
                                    <span>హైదరాబాద్‌లోని చంచల్‌గుడ జైలులో 7 రోజులు జైలు శిక్ష అనుభవించారు</span>
                                    <span>హైదరాబాద్ రాష్ట్రంలోని ఆసిఫాబాద్ జైలులో 6 నెలలు జైలు శిక్ష అనుభవించారు</span>
                                    <span>భారత స్వతంత్రసమరం లో పాల్గున్నందుకు ప్రభుత్వం నుండి తామర పత్రాన్ని  అందుకున్నారు </span>
                                    <span>భారతదేశం ప్రభుత్వం  ఫ్రీడమ్ ఫైటర్ పెన్షన్ గ్రహీత </span>
                                </p>
                            </li>
                
                            <li data-date="01/01/1980">
                                <h4 class="h4 fyellow d-none">1950-1955 </h4>
                                <h3 class="h3">విద్యావేత్త & నిర్వాహకుడు</h3>
                                <div class="row"></div>
                                <p class="listgroup">
                                    <span>మాజీ ప్రిన్సిపాల్, ప్రభుత్వం డిగ్రీ కళాశాల, జగిత్యాల్, AP</span>
                                    <span>మాజీ ప్రిన్సిపాల్, ఎస్.ఆర్.ఆర్. ప్రభుత్వం డిగ్రీ కళాశాల, కరీంనగర్, తెలంగాణ</span>
                                    <span>మాజీ వ్యవస్థాపక ప్రిన్సిపాల్, మహిళా కళాశాల, కరీంనగర్</span>
                                    <span>మాజీ ప్రిన్సిపాల్, ప్రభుత్వ నగర కళాశాల, హైదరాబాద్</span>
                                    <span>మాజీ కోఆర్డినేటర్, ఎన్టీఆర్ తెలుగు విశ్వవిద్యాలయం</span>
                                    <span>మాజీ స్పెషల్ ఆఫీసర్, ఇన్స్టిట్యూట్ ఆఫ్ ప్రొఫెషనల్ స్టడీస్, ఎన్.టి. రామారావు</span>
                                    <span>మాజీ డైరెక్టర్, శాతవాహన  ఇన్స్టిట్యూట్ ఆఫ్ పోస్ట్ గ్రాడ్యుయేట్ స్టడీస్, కరీంనగర్</span>
                                    <span>మాజీ సభ్యుడు, ది సిండికేట్ ఆఫ్ ఉస్మానియా విశ్వవిద్యాలయం, హైదరాబాద్</span>
                                    <span>మాజీ జాయింట్ డైరెక్టర్, ఉన్నత విద్య, ప్రభుత్వం ఆంధ్రప్రదేశ్</span>
                                    <span>మాజీ డైరెక్టర్, తెలుగు అకాడమీ, ఆంధ్రప్రదేశ్</span>
                                    <span>మాజీ వ్యవస్థాపక ఛైర్మన్, సిస్టర్ నివేదా కాలేజ్ ఆఫ్ ప్రొఫెషనల్ స్టడీస్, హైదరాబాద్</span>
                                    <span>వ్యవస్థాపకుడు గౌరవ. ఛైర్మన్, తెలంగాణ సాహిత్య, విద్యా, సాంస్కృతిక వేదిక</span>
                                    <span>ఫౌండర్ చైర్మన్, తెలుగు భాష పరిరక్షణ సమితి, తెలంగాణ</span>
                                    <span>ప్రస్తుతం గౌరవప్రద. ఛైర్మన్, సిస్టర్ నివేదా స్కూల్, హైదరాబాద్</span>
                                </p>
                            </li>
                
                            <li data-date="01/01/1995">
                                <h4 class="h4 fyellow d-none">1956-1965 </h4>
                                <h3 class="h3">జర్నలిస్ట్</h3>
                                <div class="row"></div>
                                <p class="listgroup">
                                    <span>ఫార్మర్ ఎడిటర్ - శాతవాహన జర్నల్ ఆఫ్ శాతవాహన ఇన్స్టిట్యూట్ ఆఫ్ పోస్ట్ గ్రాడ్యుయేట్ స్టడీస్, కరీంనగర్</span>
                                    <span>మాజీ ఎడిటర్ - ఆంధ్రప్రదేశ్ లోని తెలుగు అకాడమీ</span>
                                    <span>మాజీ ఎడిటర్ - బిజినెస్ విజన్, ఇంగ్లీష్</span>
                                    <span>ప్రస్తుత ఎడిటర్ - జయంతి, తెలుగు త్రైమాసికం</span>                                   
                                </p>
                            </li>
                
                            <li data-date="01/01/2010">
                                <h4 class="h4 fyellow d-none">1974 </h4>
                                <h3 class="h3">గౌరవ. పదవులు</h3>
                                <div class="row"></div>
                                <p class="listgroup">
                                    <span>హైదరాబాద్ పోయెట్రీ సొసైటీ మాజీ అధ్యక్షుడు</span>
                                    <span>హైదరాబాద్ ఇంటర్ భాషా కవితల సంఘం వ్యవస్థాపక అధ్యక్షుడు</span>
                                    <span>హైదరాబాద్ తెలంగాణ ఎడ్యుకేషనల్ అండ్ కల్చరల్ ఫోరం వ్యవస్థాపకుడు కన్వీనర్</span>
                                    <span>డిల్లీ లోని పోయెట్రీ సొసైటీ ఆఫ్ ఇండియా శాశ్వత సభ్యుడు</span>
                                    <span>డిల్లీ లోని ఇండియన్ ఇన్స్టిట్యూట్ ఆఫ్ పబ్లిక్ అడ్మినిస్ట్రేషన్ యొక్క శాశ్వత సభ్యుడు</span>
                                    <span>ఫోరమ్ ఫర్ హయ్యర్ ఎడ్యుకేషన్ మాజీ వైస్ చైర్మన్, హైదరాబాద్</span>
                                    <span>ప్రస్తుతం హైదరాబాద్ విశ్వనాథ సాహిత్య పీఠం ఛైర్మన్</span>
                                </p>
                            </li>
                
                            <li data-date="01/01/2025">
                                <h4 class="h4 fyellow d-none">1980 </h4>
                                <h3 class="h3">అవార్డులు</h3>
                                <div class="row"></div>
                                <p class="listgroup">
                                    <span>ఎన్టీఆర్ తెలుగు విశ్వ విద్యాలయం నుండి గౌరవ డాక్టరేట్ </span>
                                    <span>కాలిఫోర్నియాలోని ఇంటర్నేషనల్ అకాడమీ ఆఫ్ ఆర్ట్స్ నుండి గౌరవ డాక్టరేట్ </span>
                                    <span>కోల్‌కతాలోని మైఖేల్ మధుసూదన్ దత్ అకాడమీ నుండి  గౌరవ డాక్టరేట్</span>
                                    <span>సనతన ధర్మ ఛారిటబుల్ ట్రస్ట్ ఆ సద్గురు శివానంద మూర్తి నుండి ప్రతిభ పురస్కారం </span>
                                    <span>బాల్ గంగాధర్ తిలక్ కు చెందిన అమృతం కురిసినా రాత్రిని  అనువదించినందుకు ఎన్.టి.ఆర్ తెలుగు విశ్వవిద్యాలయం, హైదరాబాద్ నుండి మొదటి బహుమతి అందుకున్నారు </span>
                                    <span>దేవులపల్లి రామానుజా రావు అవార్డు అఫ్ తెలంగాణ సరస్వతి పరిషత్</span>                                   
                                </p>
                            </li>
                
                            <li data-date="01/01/2040">
                                <h3 class="h3">కవి / అనువాదకుడు</h3>
                                <div class="row"></div>
                                <p class="listgroup">
                                    <span>తెలుగు</span>
                                    <span>ఆంగ్లము </span>
                                    <span>ఉర్దూ </span>
                                    <span>పై భాషల నుండి మూడు పుస్తకాలను అనువదించారు </span>                                   
                                </p>
                            </li> 
                        </ol>
                    </div> <!-- .events-content -->
                </section>               
                <!--/ time line-->              
            </div>
            <!--/container -->
        </div>
        <!--/ section -->

        <!-- section gallery -->      
        <div class="section booksSection" id="five">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <dv class="row">
                    <!-- col -->
                    <div class="col-md-8">
                        <article>
                            <h2 class="h3 fyellow">పుస్తకాలు డా. వెల్చల కొండల్ రావు</h2>   
                                                 
                        </article>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-4 text-md-right">
                        <h2 class="h3 fyellow">36 పుస్తకాలు</h2>
                    </div>
                    <!--/ col -->
                </dv>
                <!--/row -->  

                <!-- books swiper -->
                <div class="booksSwiper">
                    <!-- Swiper -->
                    <div class="swiper-container booksList">
                        <div class="swiper-wrapper">
                            <?php 
                            for ($i=0; $i<count($bookSlider);$i++){ ?>
                            <div class="swiper-slide">
                                <img src="img/books/<?php echo $bookSlider[$i][0]?>.jpg" alt="" class="img-fluid"> 
                            </div>
                            <?php } ?>                                               
                        </div>
                        <!-- Add Pagination -->
                        <!-- <div class="swiper-pagination"></div> -->
                        <!-- Add Arrows -->
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                     <p class="pt-5 text-center">
                        <a href="javascript:void(0)" class="ybtn">అన్ని పుస్తకాలను చూడండి</a>
                    </p>   
                </div>
                <!--/ books swiper -->

            </div>
            <!--/ container -->
        </div>       
        <!--/ section books -->   

         <!-- section gallery -->      
        <div class="section gallerySection" id="seven">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <dv class="row">
                    <!-- col -->
                    <div class="col-md-8">
                        <article>
                            <h2 class="h3 fyellow">గ్యాలరీ</h2>                                                    
                        </article>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-4 text-md-right">
                        <p class="pt-2">
                            <a href="javascript:void(0)" class="ybtn">అన్ని గ్యాలరీ చూడండి</a>
                        </p>
                    </div>
                    <!--/ col -->
                </dv>
                <!--/row -->  

                <!-- swiper row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-12">
                        <!-- swiper image gallery -->
                        <div class="galSwiper">                                
                            <div class="swiper-container gallerySwiper">
                                <div class="swiper-wrapper">
                                <?php 
                                for($i=0; $i<count($galSliders); $i++){ ?>
                                    <div class="swiper-slide">
                                        <img src="img/gallery/<?php echo $galSliders[$i][0]?>.jpg" alt="" class="img-fluid">
                                    </div>   
                                <?php } ?>                                
                                </div>
                              
                                <!-- Add Pagination -->
                                 <!-- Add Arrows -->
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                                <!-- <div class="swiper-pagination"></div> -->
                            </div>                    
                        </div>
                        <!--/ swiper image gallery -->                        
                    </div>
                    <!--/ col -->
                </div>
                <!--/ swiper row -->

               
            </div>
            <!--/ container -->
        </div>       
        <!--/ section gallery -->   
        
        <!-- section contact -->       
        <div class="section contact" id="six">
            <!-- container fluid -->
            <div class="container">
                <!-- row -->
                <dv class="row">
                    <!-- col -->
                    <div class="col-md-7">
                        <article>
                            <h2 class="h3 fyellow ">మమ్మల్ని సంప్రదించండి</h2>
                            <!-- <h2 class="h3 fwhite">అందుబాటులో ఉండండి </h2>                             -->
                        </article>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-5 text-md-right">
                        <p class="pb-0 mb-0">
                            <a href="javascript:void(0)" class="fwhite">info@velchala.com</a>
                        </p>
                         <p>+918026589777</p>
                    </div>
                    <!--/ col -->
                </dv>
                <!--/row -->

               
                <form class="contactForm">
                       
            <?php     

if(isset($_POST['submitContact'])){
$to = "praveennandipati@gmail.com"; 
$subject = "Mail From ".$_POST['firstName'];
$message = "
<html>
<head>
<title>HTML email</title>
</head>
<body>
<p>".$_POST['firstName']." has sent mail!</p>
<table>
<tr>
<th align='left'>Name</th>
<td>".$_POST['firstName']."</td>
</tr>
<tr>
<th align='left'>Contact Number</th>
<td>".$_POST['userPhone']."</td>
</tr>
<tr>
<th align='left'>Email</th>
<td>".$_POST['userEmail']."</td>
</tr>
<tr>
<th align='left'>Message</th>
<td>".$_POST['msg']."</td>
</tr>
</table>
</body>
</html>
";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From:' .$_POST['firstName']. "\r\n";
//$headers .= 'Cc: myboss@example.com' . "\r\n";

mail($to,$subject,$message,$headers);   

//success mesage
?>
<div class="alert alert-success alert-dismissible fade show" role="alert">
  Mail Sent Successfully. Thank you <?= $_POST['firstName'] ?>, we will contact you shortly.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<?php

// echo "<p style='color:green'>Mail Sent. Thank you " . $_POST['name'] . ", we will contact you shortly.</p>";
}
?>
                    <!-- row -->
                    <dv class="row">
                        <!-- col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Name" required="" id="first_name" name="firstName">
                            </div>

                            <div class="form-group">
                                <input class="form-control" type="tel" placeholder="Contact No" id="phone" name="userPhone">
                            </div>

                            <div class="form-group">
                                <input class="form-control" type="email" placeholder="Email" required="" id="email" name="userEmail">
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-md-6">                          
                            <div class="form-group">
                                <textarea class="form-control" type="textarea" style="height:170px; margin-top:13px;" placeholder="Write Description" required="" id="first_name" name="msg"></textarea>
                            </div>
                        </div>
                        <!--/ col -->
                    </dv>
                    <!--/row -->
                    <div class="row">
                        <div class="col-md-6">
                            <input type="submit" class="ybtn" value="సమర్పించండి" name="submitContact">
                        </div>
                    </div>
                </form> 

                

            </div>
            <!--/ container fluid -->
        </div>        
        <!--/ section contact -->
    </div>
    <!--/ wrapper main -->

    
    <?php include 'includes/scripts.php'?>


    <!-- vsp more -->
    <!-- Modal -->
        <div class="modal fade" id="vspMore" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog fullModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title text-uppercase" id="myModalLabel">విశ్వనాథ సాహిత్య పీఠం</h4>
                </div>
                <div class="modal-body">
                   <!-- vsp header -->
                   <div class="vspHeader">
                       <img src="img/kavi_samraat.jpg" alt="">
                       <h3 class="text-uppercase">ఎ విజన్ అండ్ ప్రొజెక్షన్</h3>
                       <p>
                           <i>
                               "I am convinced also of this, the heart that must rule humanity is replaced by the mind. It is a bane"
                           </i>
                       </p>
                   </div>
                   <!--/ vsp header -->
                   <!-- vsp body -->
                   <div class="row pt-3">
                       <div class="col-md-6">
                           <p>విశ్వనాథ రచనలను సమాజంలో అందరికి అందుబాటులోకి తేవటం, దీనికనుగుణంగా సాహిత్య, సాంస్కృతిక కార్యక్రమాలు చేపట్టడం.  విశ్వనాథ గ్రంధాలయ స్థాపన: ఇందులో విశ్వనాథ రచనలు, లేఖలు, ప్రసంగాలు, ఇంటర్వ్యూలతో పాటు వారి రచనలపై వఛ్చిన పరిశోధనలు, విమర్శలు, కావ్యాలు, ప్రసంశలు, పరిశోధకులకు సహాయ ప్రొత్సాహాలందిచ్ఛే ప్రధానాంశాలతో నిక్షేపించడం. </p>

                           <p>విశ్వనాథ రచనలు ఇతర భాషల్లోకి అనువదింపచేయడం. ప్రతి సంవత్సరం తెలుగులో ఒక రచయిత/రచయిత్రిని విశ్వనాథ సాహిత్య పురస్కారంతో సన్మానించడం. దీనికోసం ప్రతిభా నిర్ణాయక సంఘాన్ని స్థాపించి ఎంపిక చేయడం. 'జయంతి' అన్న పేరున ఒక త్రైమాసిక సాహిత్య పత్రిక ప్రచురించటం, రచనల కొరకు సాహిత్యంలో పోటీలు, గోష్టులు, సమీక్షలు, సమావేశాలు నిర్వహించి వ్యాసాలు వగైరా ఎంపిక చేయడం. విశ్వనాథ రచనల పఠనా అవగాహన కొరకు ఏడాదికొకసారి రాష్ట్రంలోని వివిధ ప్రాంతాల్లో, సాహిత్య సాంస్క్రితిక సమావేశాలు, సభలు నిర్వహించడం.</p>
                       </div>
                       <div class="col-md-6">
                           <p>ఏడాదికొకసారి విశ్వనాథ సాహిత్య సృష్టిలో ఏదో ఒక ప్రక్రియపై సాహిత్య గోష్టి ఏర్పాటు ఏర్పాటు చేయటం. విశ్వనాథ సాహితీ ప్రక్రియల్లో ఏదో ఒకదానిపై ఏడాదికొకసారి వ్యాస రచన పోటీలు నిర్వహించి వారి జన్మదినోత్సవం నాడు ఉత్తమ వ్యాస రచయిత/రచయిత్రిని సన్మానించడం. విశ్వనాథ రచనల్లో ఏదో ఒక రచనపై నెలకొకసారి ఉపన్యాస కార్యక్రమం హైద్రాబాద్లో కానీ, వేరేచోట కానీ నిర్వహించడం. "కవిసామ్రాట్ విశ్వనాథ సత్యనారాయణ డీమ్డ్ యూనివర్సిటీ ఫర్ కల్చరల్ స్టడీస్" అన్నదానిని హైద్రాబాద్ లో స్థాపించడం.</p>

                           <p>విశ్వనాథ విగ్రహాన్నితదితర మహనీయుల ప్రక్కన హైద్రాబాద్లోని ట్యాంకుబండు వద్ద నెలకొల్పుటకు "రెప్రెసెంత్" చేయడం. ప్రతియేట ప్రసిద్ధుడైన ఒక పండితుణ్ణి ఆహ్వానించి విశ్వనాథ వారి జన్మదినం రోజున విశ్వనాథ స్మారక ఉపన్యాసం ఇప్పించడం. </p> 

                           <p>ఈ లక్ష్యాల సాధన కొరకు విశ్వనాథ సాహిత్య పీఠాన్ని స్థాపించడం జరిగింది.</p>
                       </div>
                   </div>
                   <!--/ vsp body -->
                </div>               
            </div>
        </div>
        </div>
    <!--/ vsp more -->

    
</body>
</html>