<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Velchala Kondal Rao</title>
    <?php 
        include 'includes/styles.php';
        include 'includes/arrayObjects.php';
    ?>
</head>
<body>
    <div class="links">
        <a id="a2" data-id="a2" href="#top"><span>Intro</span></a>
        <a id="a1" data-id="a1" href="#two"><span>VSP</span></a>
        <a id="a4" data-id="a4" href="#three"><span>Info</span></a>
        <a id="a3" data-id="a3" href="#four"><span>Career</span></a>
        <a id="a5" data-id="a5" href="#five"><span>Books</span></a>
        <a id="a7" data-id="a7" href="#seven"><span>Gallery</span></a>
        <a id="a6" data-id="a6" href="#six"><span>Contact</span></a>       
    </div>

    <!-- header -->
    <header class="fixed-top">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="indext.php">
                    <img src="img/logo.svg" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                   
                    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="indext.php">తెలుగు</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="https://www.velchala.com/publications">BOOKS</a>
                        </li>                   
                    </ul>
                    
                </div>
            </nav>
        </div>
    </header>
    <!--/ header -->

    <!-- wrapper main -->
    <div class="wrapperMain">
        <!-- section introduction -->
		<div class="section bannerSection" id="top">
            <!-- container fluid -->
            <div class="container-fluid">
                <!-- row -->
                <div class="row">
                    <!-- left col -->
                    <div class="col-sm-6 text-center">
                        <img src="img/vkraoNew.svg" alt="" class="img-fluid vkimg">
                    </div>
                    <!--/ left col -->

                    <!-- right col -->
                    <div class="col-sm-6 align-self-center">
                        <article class="vkrArticle">
                            <h1 class="h1"><span>Welcome to</span></h1>
                            <h2 class="h1">Velchala </h2>
                            <p>Dr. Rao is basically an educationist, known for his pioneering mind and promotinal drive and initiative. He has been the                   architect of many educational institutions in Andhra Pradesh. Left an indelible mark wherever he worked and whatever
                            positions he held. The last and the last but one positions he held in the State of Andhra Pradesh were Director, telugu
                            Academy and Joint Director, Higher Education. </p>
                            <!-- <p class="pt-3">
                                <a href="javascript:void(0)" class="ybtn">Know More</a>
                            </p> -->
                        </article>
                    </div>
                    <!--/ right col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container fluid -->
        </div>
        <!-- /section introduction -->

        <!-- section -->
        <div class="section bannerSection" id="two">
            <!-- container fluid -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- left col -->
                    <div class="col-sm-4 text-center order-sm-last">
                        <img src="img/vsp-png.png" alt="" class="img-fluid">
                    </div>
                    <!--/ left col -->

                    <!-- right col -->
                    <div class="col-sm-8 align-self-center">
                        <article>
                            <h2 class="h2"><span>Founder</span></h2>
                            <h2 class="h2">Viswanatha Sahitya Peetham </h2>
                            <p>A literary cum cultual organization namely "Vishwanatha Shitya Peetham" is established by "Sister Nivedita Foundation" at Red Hills, Hyderabad to function with effect from the month of july 2003.</p>
                            <p class="pt-2">
                                <a href="javascript:void(0)" class="ybtn" data-toggle="modal" data-target="#vspMore">Know More</a>
                            </p>
                        </article>
                    </div>
                    <!--/ right col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container fluid -->
        </div>
        <!--/ section -->

        <!-- section -->
        <div class="section infoSection" id="three">
            <!-- container fluid -->
            <div class="container">
                <!-- row -->
                <dv class="row">
                    <!-- col -->
                    <div class="col-md-8">
                        <article>
                            <h2 class="h3 fyellow">Lifetime Achievement Award</h2>
                            <p>Dr Velchala Kondal Rao, an efficient educationist and a prolific writer, was felicitated with Lifetime Achievement Award
                            in a function at Ravindra Bharati, Hyderabad on April 20, 2015 by a literary-cultural organisation Saampradaya in  association with the Department of Language & Culture, Government of Telangana.</p>
                        </article>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-4 text-md-right">
                        <h2 class="h3 fyellow">April 20, 2015</h2>                        
                    </div>
                    <!--/ col -->
                </dv>
                <!--/row -->

                <!-- row -->
                <dv class="row py-2">
                    <!-- col -->
                    <div class="col-md-6">
                        <article>
                            <h2 class="h4 fyellow">PERSONAL INFORMATION</h2>
                            <p>Born on 21st July, 1932 at Karimnagar, Telangana, Married to V. Nirmala Devi and Blessed with two daughters, P. Rama Devi and R. Uma Devi and a son Agam Rao Velchala</p>
                        </article>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-md-6">
                        <article>
                            <h2 class="h4 fyellow">EDUCATIONAL BACKGROUND</h2>
                            <p class="pb-0 mb-0">M. Com. from Osmania University</p>
                            <p>LL.B. from University of Lucknow</p>
                        </article>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-md-6">
                        <article>
                            <h2 class="h4 fyellow">EDITOR - COMPILER OF BOOKS</h2>
                            <p class="pb-0 mb-0">Edited 10 books in English and Telugu including a book on "Telangana Struggle for Identity"</p>
                        </article>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-md-6">
                        <article>
                            <h2 class="h4 fyellow">POET</h2>
                            <p class="pb-0 mb-0">In Languages Telugu, English, Urdu
                            </p>
                        </article>
                    </div>
                    <!--/ col -->                   
                </dv>
                <!--/row -->
            </div>
            <!--/ container fluid -->
        </div>
        <!--/ section -->

        <!-- section -->
        <div class="section" id="four">
            <!-- container-->
            <div class="container">
                <!-- row -->
                <dv class="row">
                    <!-- col -->
                    <div class="col-md-8">
                        <article>
                            <h2 class="h3 fyellow ">My Career</h2>
                            <h2 class="h3 fwhite">History & Timeline</h2>                            
                        </article>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-4 text-md-right">
                        <h2 class="h3 fyellow">75</h2>
                        <h5>Years</h5>
                    </div>
                    <!--/ col -->
                </dv>
                <!--/row -->

                <!-- time line -->
                <section class="cd-horizontal-timeline">
                    <div class="timeline">
                        <div class="events-wrapper">
                            <div class="events">
                                <ol>
                                    <li><a href="#0" data-date="01/01/1935" class="selected"><span>1932</span></a></li>
                                    <li><a href="#0" data-date="01/01/1950"><span>1937</span></a></li>
                                    <li><a href="#0" data-date="01/01/1965"><span>1945-47</span></a></li>
                                    <li><a href="#0" data-date="01/01/1980"><span>1950-55</span></a></li>
                                    <li><a href="#0" data-date="01/01/1995"><span>1956-65</span></a></li>
                                    <li><a href="#0" data-date="01/01/2010"><span>1974</span></a></li>
                                    <li><a href="#0" data-date="01/01/2025"><span>1980</span></a></li>
                                    <li><a href="#0" data-date="01/01/2040"><span>1990</span></a></li> 
                                </ol>                
                                <span class="filling-line" aria-hidden="true"></span>
                            </div> <!-- .events -->
                        </div> <!-- .events-wrapper -->
                
                        <ul class="cd-timeline-navigation">
                            <li><a href="#0" class="prev inactive">Prev</a></li>
                            <li><a href="#0" class="next">Next</a></li>
                        </ul> <!-- .cd-timeline-navigation -->
                    </div> <!-- .timeline -->
                
                    <div class="events-content">
                        <ol>
                            <li class="selected" data-date="01/01/1935">
                                <h4 class="h4 fyellow">21 July 1932 </h4>
                                <h3 class="h3">Born on 21st July, 1932 at Karimnagar, Telangana</h3>
                                <p>Born on 21st July, 1932 at Karimnagar, Telangana</p>

                            </li>                
                
                            <li data-date="01/01/1950">
                                <h4 class="h4 fyellow d-none">10 March 1954 </h4>
                                <h3 class="h3">Higher Education</h3>
                                <div class="row"></div>
                                <p class="listgroup">
                                    <span>M. Com. from Osmania University</span>
                                    <span>LL.B. from University of Lucknow</span>
                                </p>                               
                            </li>
                
                            <li data-date="01/01/1965">
                                <h4 class="h4 fyellow d-none">1945-1947 </h4>
                                <h3 class="h3">Indian Freedom Fighter</h3>
                                <div class="row"></div>
                                <p class="listgroup">
                                    <span>Worked underground during the Indian Independence Struggle and Hyderabad Liberation struggle</span>
                                    <span>Performed Satyagraha on 7th August 1947 at Kothi, Hyderabad</span>
                                    <span>Imprisoned for 7 days at Chanchalguda Jail, Hyderabad</span>
                                    <span>Imprisoned for 6 months at Asifabad Jail, Hyderabad State</span>
                                    <span>Receiver of Tamrapatra from Govt. of India for having participated in freedom struggle</span>
                                    <span>Receiver of Freedom Fighter Pension, Govt. of India</span>
                                </p>
                            </li>
                
                            <li data-date="01/01/1980">
                                <h4 class="h4 fyellow d-none">1950-1955 </h4>
                                <h3 class="h3">Educationalist & Administrator</h3>
                                <div class="row"></div>
                                <p class="listgroup">
                                    <span>Former Principal, Govt. Degree College, Jagtial, AP</span>
                                    <span>Former Principal, S.R.R. Govt. Degree College, Karimnagar, Telengana</span>
                                    <span>Former Founder Principal, Women's College, Karimnagar</span>
                                    <span>Former Principal, Government City College, Hyderabad</span>
                                    <span>Former Coordinator, NTR Telugu University</span>
                                    <span>Former Special Officer, Institute of Professional Studies, constituted by N.T. Rama Rao</span>
                                    <span>Former Director, Satavahana Institute of Post Graduate Studies, Karimnagar</span>
                                    <span>Former member, The Syndicate of Osmania University, Hyderabad</span>
                                    <span>Former Joint Director, Higher Education, Govt. of Andhra Pradesh</span>
                                    <span>Former Director, Telugu Academy, Andhra Pradesh</span>
                                    <span>Former Founder Chairman, Sister Nivedita College of Professional Studies, Hyderabad</span>
                                    <span>Founder Hon. Chairman, Telangana Literary, Educational and Cultural Forum</span>
                                    <span>Founder Chairman, Telugu Bhasha Parirakshana Samithi, Telengana</span>
                                    <span>Currently Hon. Chairman, Sister Nivedita School, Hyderabad</span>
                                </p>
                            </li>
                
                            <li data-date="01/01/1995">
                                <h4 class="h4 fyellow d-none">1956-1965 </h4>
                                <h3 class="h3">Journalist</h3>
                                <div class="row"></div>
                                <p class="listgroup">
                                    <span>Former Editor - Satavahana journal of Satavahana Institute of Post Graduate Studies, Karimnagar</span>
                                    <span>Former Editor - Telugu monthly of Telugu Academy, Andhra Pradesh</span>
                                    <span>Former Editor - Business Vision, English</span>
                                    <span>Current Editor - Jayanthi, Telugu Quarterly</span>                                   
                                </p>
                            </li>
                
                            <li data-date="01/01/2010">
                                <h4 class="h4 fyellow d-none">1974 </h4>
                                <h3 class="h3">Hon. Positions Held</h3>
                                <div class="row"></div>
                                <p class="listgroup">
                                    <span>Former President of the Poetry Society of Hyderabad</span>
                                    <span>Founder President of the Inter-lingual Poetry Society of Hyderabad</span>
                                    <span>Founder Convener of Telangana Educational and Cultural Forum, Hyderabad</span>
                                    <span>Permanent Member of the Poetry Society of India, Delhi</span>
                                    <span>Permanent Member of the Indian Institute of Public Administration, Delhi</span>
                                    <span>Former Vice Chairman of the Forum for Higher Education, Hyderabad</span>
                                    <span>Currently Chairman of the Vishwanatha Sahithya Peetam, Hyderabad</span>
                                </p>
                            </li>
                
                            <li data-date="01/01/2025">
                                <h4 class="h4 fyellow d-none">1980 </h4>
                                <h3 class="h3">Awards</h3>
                                <div class="row"></div>
                                <p class="listgroup">
                                    <span>Hon. Doctorate from N.T.R. Telugu University, Hyderabad</span>
                                    <span>Hon. Doctorate from International Academy of Arts, California</span>
                                    <span>Hon. Doctorate from Michael Madhusudan Dutt Academy, Kolkata</span>
                                    <span>Pratibha Puraskar by Sanatana Dharma Charitable Trust of Sadguru Sivananda Murthy</span>
                                    <span>Received first prize for translating Amrutham Kurisina Ratri of Bal Gangadhar Tilak from N.T.R. Telugu University,
                                    Hyderabad</span>
                                    <span>Recipient of Devulapalli Ramanuja Rao Award of Telangana Saraswathi Parishath</span>                                   
                                </p>
                            </li>
                
                            <li data-date="01/01/2040">
                                <h3 class="h3">Poet / Translator</h3>
                                <div class="row"></div>
                                <p class="listgroup">
                                    <span>Telugu</span>
                                    <span>English</span>
                                    <span>Urdu</span>
                                    <span>Translated 10 books from above Three languages</span>                                   
                                </p>
                            </li> 
                        </ol>
                    </div> <!-- .events-content -->
                </section>               
                <!--/ time line-->              
            </div>
            <!--/container -->
        </div>
        <!--/ section -->

        <!-- section gallery -->      
        <div class="section booksSection" id="five">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <dv class="row">
                    <!-- col -->
                    <div class="col-md-8">
                        <article>
                            <h2 class="h3 fyellow">Books by Dr. Velchala Kondal Rao</h2>   
                                                 
                        </article>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-4 text-md-right">
                        <h2 class="h3 fyellow">36 Books</h2>
                    </div>
                    <!--/ col -->
                </dv>
                <!--/row -->  

                <!-- books swiper -->
                <div class="booksSwiper">
                    <!-- Swiper -->
                    <div class="swiper-container booksList">
                        <div class="swiper-wrapper">
                            <?php 
                            for ($i=0; $i<count($bookSlider);$i++){ ?>
                            <div class="swiper-slide">
                                <img src="img/books/<?php echo $bookSlider[$i][0]?>.jpg" alt="" class="img-fluid"> 
                            </div>
                            <?php } ?>                                               
                        </div>
                        <!-- Add Pagination -->
                        <!-- <div class="swiper-pagination"></div> -->
                        <!-- Add Arrows -->
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                     <p class="pt-5 text-center">
                        <a href="javascript:void(0)" class="ybtn">View All Books</a>
                    </p>   
                </div>
                <!--/ books swiper -->

            </div>
            <!--/ container -->
        </div>       
        <!--/ section books -->   

         <!-- section gallery -->      
        <div class="section gallerySection" id="seven">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <dv class="row">
                    <!-- col -->
                    <div class="col-md-8">
                        <article>
                            <h2 class="h3 fyellow">Gallery</h2>                                                    
                        </article>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-4 text-md-right">
                        <p class="pt-2">
                            <a href="javascript:void(0)" class="ybtn">View More</a>
                        </p>
                    </div>
                    <!--/ col -->
                </dv>
                <!--/row -->  

                <!-- swiper row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-12">
                        <!-- swiper image gallery -->
                        <div class="galSwiper">                                
                            <div class="swiper-container gallerySwiper">
                                <div class="swiper-wrapper">
                                <?php 
                                for($i=0; $i<count($galSliders); $i++){ ?>
                                    <div class="swiper-slide">
                                        <img src="img/gallery/<?php echo $galSliders[$i][0]?>.jpg" alt="" class="img-fluid">
                                    </div>   
                                <?php } ?>                                
                                </div>
                              
                                <!-- Add Pagination -->
                                 <!-- Add Arrows -->
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                                <!-- <div class="swiper-pagination"></div> -->
                            </div>                    
                        </div>
                        <!--/ swiper image gallery -->                        
                    </div>
                    <!--/ col -->
                </div>
                <!--/ swiper row -->

               
            </div>
            <!--/ container -->
        </div>       
        <!--/ section gallery -->   
        
        <!-- section contact -->       
        <div class="section contact" id="six">
            <!-- container fluid -->
            <div class="container">
                <!-- row -->
                <dv class="row">
                    <!-- col -->
                    <div class="col-md-7">
                        <article>
                            <h2 class="h3 fyellow ">Contact us</h2>
                            <h2 class="h3 fwhite">Let's Get in Touch</h2>                            
                        </article>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-5 text-md-right">
                        <p class="pb-0 mb-0">
                            <a href="javascript:void(0)" class="fwhite">info@velchala.com</a>
                        </p>
                         <p>+918026589777</p>
                    </div>
                    <!--/ col -->
                </dv>
                <!--/row -->

                <form class="contactForm">
                       
            <?php     

if(isset($_POST['submitContact'])){
$to = "praveennandipati@gmail.com"; 
$subject = "Mail From ".$_POST['firstName'];
$message = "
<html>
<head>
<title>HTML email</title>
</head>
<body>
<p>".$_POST['firstName']." has sent mail!</p>
<table>
<tr>
<th align='left'>Name</th>
<td>".$_POST['firstName']."</td>
</tr>
<tr>
<th align='left'>Contact Number</th>
<td>".$_POST['userPhone']."</td>
</tr>
<tr>
<th align='left'>Email</th>
<td>".$_POST['userEmail']."</td>
</tr>
<tr>
<th align='left'>Message</th>
<td>".$_POST['msg']."</td>
</tr>
</table>
</body>
</html>
";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From:' .$_POST['firstName']. "\r\n";
//$headers .= 'Cc: myboss@example.com' . "\r\n";

mail($to,$subject,$message,$headers);   

//success mesage
?>
<div class="alert alert-success alert-dismissible fade show" role="alert">
  Mail Sent Successfully. Thank you <?= $_POST['firstName'] ?>, we will contact you shortly.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<?php

// echo "<p style='color:green'>Mail Sent. Thank you " . $_POST['name'] . ", we will contact you shortly.</p>";
}
?>
                    <!-- row -->
                    <dv class="row">
                        <!-- col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Name" required="" id="first_name" name="firstName">
                            </div>

                            <div class="form-group">
                                <input class="form-control" type="tel" placeholder="Contact No" id="phone" name="userPhone">
                            </div>

                            <div class="form-group">
                                <input class="form-control" type="email" placeholder="Email" required="" id="email" name="userEmail">
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-md-6">                          
                            <div class="form-group">
                                <textarea class="form-control" type="textarea" style="height:170px; margin-top:13px;" placeholder="Write Description" required="" id="first_name" name="msg"></textarea>
                            </div>
                        </div>
                        <!--/ col -->
                    </dv>
                    <!--/row -->
                    <div class="row">
                        <div class="col-md-6">
                            <input type="submit" class="ybtn" value="Submit" name="submitContact">
                        </div>
                    </div>
                </form>                

            </div>
            <!--/ container fluid -->
        </div>        
        <!--/ section contact -->
    </div>
    <!--/ wrapper main -->

    
    <?php include 'includes/scripts.php'?>


    <!-- vsp more -->
    <!-- Modal -->
        <div class="modal fade" id="vspMore" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog fullModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title text-uppercase" id="myModalLabel">Viswanatha Sahitya Peetham</h4>
                </div>
                <div class="modal-body">
                   <!-- vsp header -->
                   <div class="vspHeader">
                       <img src="img/kavi_samraat.jpg" alt="">
                       <h3 class="text-uppercase">A Vision and a Projection</h3>
                       <p>
                           <i>
                               "I am convinced also of this, the heart that must rule humanity is replaced by the mind. It is a bane"
                           </i>
                       </p>
                   </div>
                   <!--/ vsp header -->
                   <!-- vsp body -->
                   <div class="row pt-3">
                       <div class="col-md-6">
                           <p>The primary objective of the “Peetam” is to acquaint more people with Viswanadha’s genius and Works by organizing such literary and cultural meets and activities as may be needed. To establish a library with books by and on Viswanadha, including audio video material, letters, radio talks and the like to facilitate the research work of scholars doing research on Viswanadha, and also to arrange such guidance and counselling as may be needed by the research scholars. </p>

                           <p>To encourage translations of Viswanadha’s works into other languages. To institute an award namely “Viswanadha Award” to be given to an eminent writer in Telugu adjudged as such by the jury constituted for the purpose. To start a quarterly by name “JAYANTHI“ to publish literary views and reviews. To conduct cultural and literary programs once in a year.</p>
                       </div>
                       <div class="col-md-6">
                           <p>To hold a seminar once a year on one of the literary aspects of Viswanadha. To hold an essay writing competition once a year on one of Viswanadha’s literary aspects and to award prizes. To hold a meeting once a month at Hyderabad or at any other place to discuss about one of the books of Viswanadha. To help to establish a Deemed University namely “Kavisamrat Viswanadha Satyanarayana Deemed University for Literary and Cultural Studies” at Hyderabad.</p>

                           <p>To arrange a Viswanadha Memorial Lecture by an eminent scholar every year on his birthday. For the purpose of achieving the above objectives a literary and cultural organization namely “Viswanatha Sahitya Peetam” for the promotion of Viswanadha’s Thoughts.</p>
                       </div>
                   </div>
                   <!--/ vsp body -->
                </div>               
            </div>
        </div>
        </div>
    <!--/ vsp more -->

    
</body>
</html>